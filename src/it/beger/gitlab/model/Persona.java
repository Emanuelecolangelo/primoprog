package it.beger.gitlab.model;

public class Persona {
	
	private String nome;
	private String indirizzo;
	private int eta;
	
	public Persona(String nome, String indirizzo, int eta) {
		
		
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.eta = eta;

}
	
	public Persona() {
	}

	
	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getIndirizzo() {
		return indirizzo;
	}


	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}


	public int getEta() {
		return eta;
	}


	public void setEta(int eta) {
		this.eta = eta;
	}


	@Override
	public String toString() {
		return "Persona [nome=" + nome + ", indirizzo=" + indirizzo + ", eta=" + eta + "]";
	}
	

}
