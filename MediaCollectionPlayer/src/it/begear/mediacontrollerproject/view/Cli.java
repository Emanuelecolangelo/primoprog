package it.begear.mediacontrollerproject.view;

public class Cli {

	public static void welcome() {
		System.out.println("Benvenuto");
	}
	public static void goodbye() {
		System.out.println("Goodbye");
	}
	
	public static void options() {
		System.out.println("Digita un numero per effettuare un'operazione:");
		System.out.println("1. Inserisci Oggetto");
		System.out.println("2. Leggi Oggetto");
		System.out.println("3. Elimina Oggetto");
		System.out.println("4. Leggi collezione");
		System.out.println("5. Esci");
	}
	
	public static void instance() {
		System.out.println("Che tipo di oggetto vuoi instanziare?");
		System.out.println("1. File Audio");
		System.out.println("2. File Video");
		System.out.println("3. File Ebook");
		
	}

}
