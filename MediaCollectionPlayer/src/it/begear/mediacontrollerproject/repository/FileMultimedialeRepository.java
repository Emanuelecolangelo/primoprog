package it.begear.mediacontrollerproject.repository;

import it.begear.mediacontrollerproject.model.FileMultimediale;

public class FileMultimedialeRepository {

	private FileMultimediale array[];
	private int index = 0;

	public FileMultimedialeRepository(int size) {
		this.array = new FileMultimediale[size];
	}

	public void save(FileMultimediale file) {
		if (array != null) {
			array[index++] = file;
		}
	}

	public void readAll() {
		for (int i = 0; i < index; i++) {
			System.out.println(array[i]);
		}
	}

	public void read(String titolo) {

		for (int i = 0; i < index; i++) {
			if(array[i].getTitolo().toLowerCase().equals(titolo)) {

				System.out.println(array[i]);
				break;
			}
		}
	}
	public void delete(String titolo) {

		for (int i = 0; i < index; i++) {
			if(array[i].getTitolo().toLowerCase().equals(titolo)) {

				array[i] = null;
				break;
			}
		}
	}

}
