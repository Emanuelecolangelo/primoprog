package it.begear.mediacontrollerproject.controller;

import it.begear.mediacontrollerproject.model.Audio;
import it.begear.mediacontrollerproject.model.Ebook;
import it.begear.mediacontrollerproject.model.Video;
import it.begear.mediacontrollerproject.repository.FileMultimedialeRepository;
import it.begear.mediacontrollerproject.service.AudioServiceImpl;
import it.begear.mediacontrollerproject.service.EbookServiceImpl;
import it.begear.mediacontrollerproject.service.VideoServiceImpl;
import it.begear.mediacontrollerproject.util.ScannerManager;
import it.begear.mediacontrollerproject.view.Cli;

public class MediaCollectionPlayer {

	private final static FileMultimedialeRepository repo = new FileMultimedialeRepository(50);

	public static void main(String[] args) {

		int key = 0;
		Cli.welcome();

		while (key != 5) {

			// Stampa del menu e get del valore di key
			Cli.options();
			key = ScannerManager.getInteger();

			// Gestione Menu
			switch (key) {
			case 1:
				insertObject();
				break;
			case 2:
				readObject();
				break;
			case 3:
				deleteObject();
				break;
			case 4:
				repo.readAll();
				break;
			case 5:
				Cli.goodbye();
				break;

			default:
				break;
			}

		}

	}

	private static void insertObject() {
		// Get indice da input
		Cli.instance();
		int key_1;
		key_1 = ScannerManager.getInteger();

		// Gestione istanze
		switch (key_1) {
		case 1:
			// Nuovo File Audio
			Audio audio = new AudioServiceImpl().popolaElemento();
			repo.save(audio);
			break;

		case 2:
			Video video = new VideoServiceImpl().popolaElemento();
			repo.save(video);
			break;

		case 3:
			Ebook ebook = new EbookServiceImpl().popolaElemento();
			repo.save(ebook);
			break;

		default:
			break;
		}

	}

	private static void readObject() {
		System.out.println("Inserisci il titolo dell'oggetto che vuoi leggere");
		repo.read(ScannerManager.getString().toLowerCase());

	}

	private static void deleteObject() {
		System.out.println("Inserisci il titolo dell'oggetto che vuoi eliminare");
		repo.delete(ScannerManager.getString().toLowerCase());

	}
}
