package it.begear.mediacontrollerproject.model;

public class Video extends FileMultimediale {
	
	private Integer durata;
	
	public Video() {
		super();
	}

	public Video(String titolo, String autore, String genere, int dimensione,
			int anno, int mese, int giorno, int durata) {
		super(titolo, autore, genere, anno, mese, giorno);

		this.durata = durata;
	}

	public Integer getDurata() {
		return durata;
	}

	public void setDurata(Integer durata) {
		this.durata = durata;
	}

	@Override
	public String toString() {
		return "Video [" + super.toString()  + "]";
	}

	@Override
	public void play() {

		System.out.println("Stai guardando il film " + this.getTitolo());
		
	}

	@Override
	public void stop() {

		System.out.println("Hai fermato il film " + this.getTitolo());
		
	}

}
