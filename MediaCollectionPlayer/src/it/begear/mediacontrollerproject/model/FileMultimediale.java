package it.begear.mediacontrollerproject.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public abstract class FileMultimediale implements FileMultimedialePlayer {
	private String genere;
	private String titolo;
	private String autore;
	private Calendar dataPubblicazione;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	// TODO private String estensione da aggiungere come enum

	public FileMultimediale() {
		
		this.dataPubblicazione =  Calendar.getInstance(TimeZone.getTimeZone("CET"));
	}

	public FileMultimediale(String titolo, String autore, String genere, int anno, int mese, int giorno) {
		this.titolo = titolo;
		this.autore = autore;
		this.genere = genere;
		this.dataPubblicazione = Calendar.getInstance(TimeZone.getTimeZone("CET"));
		dateFormat.setCalendar(this.dataPubblicazione); 
		this.dataPubblicazione.set(anno, mese, giorno);
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public Calendar getDataPubblicazione() {
		return dataPubblicazione;
	}

	public void setDataPubblicazione(int giorno, int mese, int anno) {
		this.dataPubblicazione.set(anno, mese, giorno);

	}

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	@Override
	public String toString() {
		return "Titolo=" + titolo + ", Autore=" + autore + ", Genere="+ genere +", Data=" + dateFormat.format(this.dataPubblicazione.getTime()) + "]";
	}

}
