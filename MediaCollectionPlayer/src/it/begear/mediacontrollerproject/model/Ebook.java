package it.begear.mediacontrollerproject.model;

public class Ebook extends FileMultimediale {
	
	private Integer numeroPagine;
	
	public Ebook() {
		super();
	}

	public Ebook(String titolo, String autore, String genere, int dimensione,
			int anno, int mese, int giorno, Integer numeroPagine) {
		super(titolo, autore, genere, anno, mese, giorno);

		this.numeroPagine = numeroPagine;
	}

	public Integer getNumeroPagine() {
		return numeroPagine;
	}

	public void setNumeroPagine(Integer numeroPagine) {
		this.numeroPagine = numeroPagine;
	}

	@Override
	public String toString() {
		return "Ebook [" + super.toString()  + "]";}

	@Override
	public void play() {

		System.out.println("Stai leggendo il libro " + this.getTitolo());
		
	}

	@Override
	public void stop() {

		System.out.println("Non stai più leggendo il libro " + this.getTitolo());
		
	}
	
	

}
