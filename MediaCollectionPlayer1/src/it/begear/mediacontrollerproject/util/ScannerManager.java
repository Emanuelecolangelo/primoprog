package it.begear.mediacontrollerproject.util;

import java.util.Scanner;

public class ScannerManager {

	public static Integer getInteger() {
		Scanner scanner = new Scanner(System.in);
		
		if(scanner.hasNextInt()) {
			
			return scanner.nextInt();
		}else {
//			String flush = scanner.nextLine();
			System.out.println("Riprova inserimento di un intero");
			return getInteger();
		}
	}
	
	public static String getString() {
		Scanner scanner = new Scanner(System.in);
		
		if(scanner.hasNext()) {
			
			return scanner.nextLine();
		}else {
//			String flush = scanner.nextLine();
			System.out.println("Riprova inserimento di un intero");
			return getString();
		}
	}

}
