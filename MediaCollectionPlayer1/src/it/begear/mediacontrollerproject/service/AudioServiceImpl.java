package it.begear.mediacontrollerproject.service;

import it.begear.mediacontrollerproject.model.Audio;
import it.begear.mediacontrollerproject.util.ScannerManager;

public class AudioServiceImpl{

	public Audio popolaElemento() {
		
		Audio audio = new Audio();
		
		audio = (Audio) new FileMultimedialeService().saveElement(audio);
		
		System.out.println("Inserisci Frequenza");
		audio.setFrequenza(ScannerManager.getInteger());

		return audio;
		
		
	}

}
