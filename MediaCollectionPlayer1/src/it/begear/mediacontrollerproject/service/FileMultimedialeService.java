package it.begear.mediacontrollerproject.service;

import it.begear.mediacontrollerproject.model.FileMultimediale;
import it.begear.mediacontrollerproject.util.ScannerManager;

public class FileMultimedialeService{

	public FileMultimediale saveElement(FileMultimediale file) {
		
	System.out.println("Inserisci Titolo");
	file.setTitolo(ScannerManager.getString());
	System.out.println("Inserisci Autore");
	file.setAutore(ScannerManager.getString());
	System.out.println("Inserisci Giorno pubblicazione");
	int giorno = ScannerManager.getInteger();
	System.out.println("Inserisci Mese pubblicazione");
	int mese = ScannerManager.getInteger();
	System.out.println("Inserisci Anno pubblicazione");
	int anno = ScannerManager.getInteger();
	file.setDataPubblicazione(giorno, mese, anno);
	System.out.println("Inserisci Genere");
	file.setGenere(ScannerManager.getString());
	
	return file;

	}

}
