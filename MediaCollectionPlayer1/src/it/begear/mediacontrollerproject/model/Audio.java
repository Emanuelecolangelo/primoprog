package it.begear.mediacontrollerproject.model;

import java.text.SimpleDateFormat;

public class Audio extends FileMultimediale{
	 //TODO enum genere
	private Integer frequenza; // TODO enum frequenza
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	public Audio() {

		super();
	}

	public Audio(String titolo, String autore, int dimensione, int anno, int mese, int giorno, String genere, int frequenza) {
		super(titolo, autore, genere, anno, mese, giorno);
		this.frequenza = frequenza;
	}

	public int getFrequenza() {
		return frequenza;
	}

	public void setFrequenza(int frequenza) {
		this.frequenza = frequenza;
	}

	@Override
	public String toString() {
		return "Audio [" + super.toString()  + "]";
	}

	@Override
	public void play() {

		System.out.println("Stai riproducendo il brano " + this.getTitolo());
	}

	@Override
	public void stop() {
		System.out.println("Hai fermato il brano " + this.getTitolo());
		
	}

	
}
