package it.begear.mediacontrollerproject.model;

public interface FileMultimedialePlayer {
	
	void play();
	void stop();	

}
